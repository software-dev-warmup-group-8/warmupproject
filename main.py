from input_parser import parse_input
from operations import Engine

def main():
    engine = Engine("group8-warmup-firebase-adminsdk.json")
    inp = ""

    # While input is not quit
    while(inp != "quit"):

        # Get input from user
        inp = input("> ")

        # Check if input == quit
        if(inp.strip().lower() == "quit"):
            break

        # Check if input == help
        # Display canned help text
        if (inp.strip().lower() == "help"):
            print("="*10 + " help menu " + "="*10)
            print("***valid parameter inputs***")
            print("'director' 'year' 'runtime' 'stars' 'rating' 'series'")
            print("***valid operand inputs***")
            print("'==' '>' '<' '>=' '<=', also supports compouding inequalities with 'and'")
            print("***example valid inputs***")
            print("'director of \"Inception\"' 'series of \"The Godfather\"' 'year > 1990' 'rating == \"R\"'")
            print("'year > 1990 and rating == \"R\"")

        # Otherwise, we can pass to string to parser function
        else:
            parse = parse_input(inp)
            if (parse is not None):
                # Parse is in language and can be sent to query engine
                print(engine.formatted_query(parse))
                print('\n')

if __name__=="__main__":
    main()
