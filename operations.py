import json
import uuid

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from google.cloud.firestore_v1.base_query import FieldFilter

class Movie:
    # Initializes a movie with data in all fields
    def __init__ (self, name, director, year, runtime, rating, stars, series):
        self.name = name
        self.director = director
        self.year = year
        self.runtime = runtime
        self.rating = rating
        self.stars = stars
        self.series = series

    # Converts dictionary output from query to Movie object
    def from_dict(source, series):
        keys = source.keys()
        name = source["name"]
        director = source["director"]
        year = source["year"]
        runtime = source["runtime"]
        stars = source["stars"]

        rating = "No Rating"
        if "rating" in keys:
            rating = source["rating"]
        
        if series == []:
            series = "No series movies"

        return Movie(name, director, year, runtime, rating, stars, series)

    # Returns a string representation of a movie
    def to_string(self):
        director_string = [entry['name'] for entry in self.director]
        director_string = ', '.join(director_string)
        stars_string = [entry['name'] for entry in self.stars]
        stars_string = ', '.join(stars_string)

        s = '"' + self.name + '"' + "\nDirector: " + director_string + "\nYear: " + str(self.year) + "\nRuntime(in minutes): " + str(self.runtime)

        if (self.rating is not None):
            s = s + "\nRating: " + self.rating + "\nStars: " + stars_string
        else:
            s = s + "\nRating: Unrated" + "\nStars: " + stars_string
        
        # Append series movies in form "[name]([position])" ex: The Godfather part II(2)
        if (self.series != "No series movies"):
            s = s + "\nSeries: " + '\n'.join([f"{entry['name']}({entry['position']})" for entry in self.series])
        return s

class Engine:
    # Initialize Engine object, authenticate access to the database
    def __init__(self, cred_path):
        self.cred = credentials.Certificate(cred_path)
        self.app = firebase_admin.initialize_app(self.cred)
        self.db = firestore.client()
        self.coll = self.db.collection('movie-data')

    # Upload data form a given json file to the database
    # No returns
    def upload_data(self, json_path):
        with open(json_path, 'r') as json_file:
            data = json.load(json_file)
        
        # Iterate through documents in the json
        for item in data:
            doc_uuid = uuid.uuid4().hex
            doc_ref = self.coll.document(doc_uuid)

            # Create subcollection of other movies in series
            if "series" in item.keys():
                for sub_item in item["series"]:
                    sub_uuid = uuid.uuid4().hex
                    sub_doc_ref = doc_ref.collection("series").document(sub_uuid)
                    sub_doc_ref.set(sub_item)
                
                del item["series"]
            doc_ref.set(item)

    # Delete all current documents from the database
    # No returns
    def clear_db(self, batch_size):
        docs =  self.coll.list_documents(batch_size)

        for doc in docs:
            sub_docs = doc.collection("series").list_documents(50)
            for sub_doc in sub_docs:
                sub_doc.delete()
            doc.delete()

    # Handles queries on the subcollection "series"
    # returns a list of strings
    def subcol_query(self, args):
        collection = self.coll.stream()
        output = []

        for doc in collection:
            in_query = False
            series = []
            sub_docs = self.coll.document(doc.id).collection("series").stream()
            for sub_doc in sub_docs:
                sub_doc_dict = sub_doc.to_dict()
                series.append(sub_doc_dict)
                if sub_doc_dict["name"] == args[2]:
                    in_query = True
            
            if in_query:
                output.append(Movie.from_dict(doc.to_dict(), series).to_string())

        return output
    
    # Handles queries on array fields
    # returns a stream of documents
    def array_query(self, args):
        dict_arg = {"name": args[2]}
        query = self.coll.where(filter=FieldFilter(args[0], "array_contains", dict_arg)).stream()
        return query
    
    # Handles queries of the form [field] of [movie] ex: 'Director of "Inception"'
    # returns a string
    def field_query(self, args):
        query = self.coll.where(filter=FieldFilter("name", "==", args[1])).stream()
        output = "No such movie"

        for doc in query:
            series = []
            sub_docs = self.coll.document(doc.id).collection("series").stream()
            for sub_doc in sub_docs:
                series.append(sub_doc.to_dict())

            # Get requested attribute from movie object
            movie = Movie.from_dict(doc.to_dict(), series)
            output = getattr(movie, args[0])

            # Output formatting for list based outputs
            if args[0].lower() == "series" and output != "No series movies":
                output = [f"{entry['name']}({entry['position']})" for entry in output]
                output = ', '.join(output)
            elif args[0].lower() == "director" or args[0].lower() == "stars":
                output = [entry['name'] for entry in output]
                output = ', '.join(output)
        
        return output
        
    # Make a query with the database
    # Returns list of strings
    def query(self, args):
        output = []        
        if (len(args) == 3):
            # Call appropriate handler if needed
            if (args[0] == "director" or args[0] == "stars"):
                query = self.array_query(args)
            elif (args[0] == "series"):
                output = self.subcol_query(args)

                if output == []:
                    output.append("No such movie")

                return output
            else:
                query = self.coll.where(filter=FieldFilter(args[0],args[1],args[2])).stream()

            # Convert data stream to list of strings
            for doc in query:
                series = []
                sub_docs = self.coll.document(doc.id).collection("series").stream()
                for sub_doc in sub_docs:
                    series.append(sub_doc.to_dict())

                output.append(Movie.from_dict(doc.to_dict(), series).to_string())

        # Make recursive calls for compound queries    
        else:
            # Compound query ex: "year > 1990 and rating == 'R'"
            left_list = self.query(args[:3])
            right_list = self.query(args[3:])

            for movie in left_list:
                if movie in right_list:
                    output.append(movie)


        if output == []:
             output.append("No such movie")

        return output

    # Highest level query, calls the other query functions
    # Returns a string
    def formatted_query(self, args):
        data = []
        if (len(args) == 2):
            field = self.field_query(args)
            return field
        else:
            data = self.query(args)
        
        out_str = ""

        if type(data) == str:
            return data

        for movie in data:
            out_str += movie + "\n\n"
        
        return out_str