import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import json
import uuid

def authenticate():
    
    cred = credentials.Certificate("group8-warmup-c5497163cdf1.json")

    firebase_admin.initialize_app(cred)

    db = firestore.client()

    return db
