# input_parser.py

# join_splits(arr)
# in  -> array of strings with quote characters in any two elements
# out -> new array, any string in the original array with a quote character or 
#        in between two strings with quote characters inside is concatenated
#        into one object
def join_splits(arr):
    # Initialization of empty return values and boolean flags
    rtnval = []
    first_quote_found = False
    second_quote_found = False
    new_string = ""

    for i in range(len(arr)):
        if not first_quote_found:
            # Case where quotes are already in same segment
            if arr[i][0] == '"' and arr[i][-1] == '"':
                rtnval.append(arr[i][1:-1])
            # Case where segment with opening quotes is found
            elif arr[i][0] == '"':
                first_quote_found = True
                new_string += arr[i][1:]
            # First quote still not found
            else:
                rtnval.append(arr[i])
        else:
            # Second quote found, close new string and append to original list
            if arr[i][-1] == '"':
                first_quote_found = False
                new_string += " " + arr[i][:-1]

                rtnval.append(new_string)
                new_string = ""
            # Second quote not reached yet
            else:
                new_string += " " + arr[i]
    return rtnval 

# parse_input(string s)
# in  -> string
# out -> array of strings, either 2, 3, or 6 elements if query is valid
#     -> returns None if misinput or help functionality used
#     -> returns one output string "quit" if quit functionakity is used
def parse_input(s):
    # Lists of various tokens to check for
    field_tokens = ["name", "director", "year", "runtime", "rating", "stars", "series"]
    relational_tokens = ["<", ">", "==", "<=", ">="]

    # Empty list to add return values to
    parameters = []

    # Handle the case of too many or too few quotes
    if ((s.count('"') % 2) != 0):
        print("Too few or too many quotes\n")
        return

    # The built in split() function can return a list of strings delimited by
    # a specific characters (or whitespace if left blank)
    segments = s.split()
    segments = join_splits(segments)

    # Change numbers from strings into integers
    for i in range(len(segments)):
        if segments[i].isnumeric():
            segments[i] = int(segments[i])

    # Any of the lengths below, after the segments have been joined, would 
    # indicatea query prompt that is out of the language
    if len(segments) == 4 or \
       len(segments) == 5 or \
       len(segments) == 6 or \
       len(segments) < 2 or \
       len(segments) > 7:
        print("Your prompt is too long or too short\n")
        return

    ### Specific Field Query
    # Every query of this type will contian a field and a document name
    # delimited by the word "of", thus we can split the string and return the 
    # segments around the "of"
    elif segments[0].lower() in field_tokens and segments[1].lower() == "of":
        parameters.append(segments[0].lower())
        parameters.append(segments[2])
        return parameters

    ### Non-Optional Sublist Query
    elif segments[0].lower() == "starring":
        parameters.append("stars")
        parameters.append("==")
        parameters.append(segments[1])
        return parameters

    ### Relational Queries (Compound & Non-Compound)
    elif segments[1] in relational_tokens and segments[0].lower() in field_tokens:
        if "and" in segments and segments[5] in relational_tokens:
            # Out of language comparison 
            if segments[0] not in ["year", "runtime", "rating"]:
                if segments[1] != "==":
                    print("You cant compare that field with an inequality\n")
                    return
            if segments[4] not in ["year", "runtime", "rating"]:
                if segments[5] != "==":
                    print("You cant compare that field with an inequality\n")
                    return
            # In language comparison
            parameters.append(segments[0].lower())
            parameters.append(segments[1])
            parameters.append(segments[2])
            parameters.append(segments[4].lower())
            parameters.append(segments[5])
            parameters.append(segments[6])
            return parameters
        else:
            # Out of language comparison
            if segments[0] not in ["year", "runtime", "rating"]:
                if segments[1] != "==":
                    print("You cant compare that field with an inequality\n")
                    return
            # In language comparison
            parameters.append(segments[0].lower())
            parameters.append(segments[1])
            parameters.append(segments[2])
            return parameters

    ### No valid tokens have been identified once this line is reached
    # funciton will return None
    # >if parse_input() is None will return True
    else:
        print("Sorry, your query doesn't seem to match the language\n")
        return
