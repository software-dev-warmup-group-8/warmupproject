from operations import Engine

def main():
    data_json = "movie-data.json"
    cred_json = "group8-warmup-firebase-adminsdk.json"
    engine = Engine(cred_json)

    # Clears database
    engine.clear_db(50)

    # uploads data stored in data_json
    engine.upload_data(data_json)

main()